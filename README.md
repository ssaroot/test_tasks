https://github.com/linagora/james-jenkins/blob/master/create-dsl-job.groovy

Check this: https://github.com/flugel-it/fun-with-jenkins2
We need to finish it. 

It's based on the official Jenkins Docker image. Requirements:Create your fork to finish.

After you build and run the image, it should be ready to log into Jenkins using admin/admin. 
I think this is done, but test it.Then we seed to setup the image for autoconfiguration. 
It should create a job using groovy scripting that executes the "echo helloworld" command.
I think you have all the pieces there to assemble, but do some research.
The result of this should be code in you forked repo, with the instructions to build and run that 
in my Linux desktop.

1. Clone repository: git clone -b dev git@bitbucket.org:ssaroot/test_tasks.git
2. Install Docker, see https://docs.docker.com/engine/getstarted/step_one/
3. Enter repository folder
4. Build docker container: "docker build -t localhost/jenkins -f ./Dockerfile ."
5. Run docker container: "docker run -d -t localhost/jenkins --name jenkins0"
6. Enter to docker container as root: "docker exec -u 0 -i -t jenkins0 bash"
7. Run "ip addr sh" for see ip addres
8. Open web browser and enter url docker_ip:8080
9. Login like admin/admin
10. create new job(pipline)
11. Put pipeline script from Jenkinsfile, save it
12. Run job